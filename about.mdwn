#About

##Purpose
We are humans and might as well get used to it. So far, remotely done power and glory—as via government, big business, formal education, church—has succeeded to the point where gross profits obscure actual loss. In response to this dilemma and to these losses a realm of intimate, community power is developing—power of communities to conduct their own education, find their own inspiration, shape their own environment, and share their knowledge with others. Practices that aid this process are sought and promoted by the DAMAGED EARTH CATALOG.

##Function
The DAMAGED EARTH CATALOG functions as an evaluation and access device. With it, the visitor can discover what they might want to learn more about and where and how to do this learning.

An item is listed in the CATALOG if it is deemed:  
1. Useful as a practice,  
2. Relevant to independent education,  
3. High quality and low cost,  
4. Forgotten common knowledge,  
5. Easily available by mail.  

This information is continually revised according to the experience and suggestions of CATALOG readers and writer. 

<code>

    
                                       ,,,╓╓╓,,,,
                                ╓▄Φ▓▓╫╫╫▓▓
                            ╓Φ▓▓▓▓▓▓╫╫╫╫▓╫▓▓▓╫╫
                         ▄▓▓╫╫▓▓▓▓╫▓▀╬▀▓██▓╨`╓▄
                      ,▄█╫╫╬▓█████▌╣▓█▓█▀░gH
                    ,▓██▓▓█████████████▓▓M▄▓█
                   ▄██▓▓█████▓█▓▀▀▀╣Ñ▄▓███▓█▀▀██
                  ▓██▓███▓▓██▓╫▄▄╣█████████▌░╙╨╬▓
                 ▓████╫╨░╬▓▓██████▓█████████▓▓╗`╙▓█▓
                ▓███████▓╫▓██████████████████▄╠φΦ▓▓▌▀▀
               ╒▓███████▌╠▄╫▓▌ÜÖ████████████
               ╢▓╫╫╫╫▓▓╣▓▓▓▓╬╫▓███████▓▓█▓█
               ▓╫▓▓╫░╫▓╦╫▀▓▓╫▓▓███▓██▓▀▓╬▓▓▓
               ▓▓╫╫╫╫╫▓▓▓▓▓▓▓██████▓▓w╦~╫╨^*`╙╣
               ▓▓▓╬╣▓▓╫░╣▓▓▓▓███▓▓▓▓█▓NM░]^;~'"╫
               ▐▓▓╫▓▓▓█Ñ`▀Ñ▀▀▄,"╨╨╫▓╫╫╫╬H  «"" `"╫▓
                ▓▓╫╩▀╣░╙ »"╨╬▓█▓▓▓▓▓▓▓▓▓╔╣▓,.   `
                 ▓▓▒▓▄▄╗╦╫╫╦╦▄░╠▓▓▓███▓▓███▓▄░░╔╬╦, `╚
                 `█▓▓▓█▓▓▓▓▓▓▓▓╫╫▓████████████▓
                   █▓▄╨▀█▓╫╫╫▓▓████████████
                    ╙█▓▄▄▀╫╦╬╫▓██▌▓██▄` ╙▀
                      ╙▓▓▒╟╫╫▄▓▓██║▓██▓╦w   `
                        ╙╣Ñ╫╫▓▀▀▓▓░▀▓████▓▄  ¡╙
                           ╙▀▓▓╫╫╬▄φ╣▓███Ñ╙▀  ``░ù░Å▀╫╫
                              `╙╩▀▓▓▓╫▓╫╫╫╦╦╦Ñññ╨░░╨░╠
                                    `""*╨══╩══╩╨╨^`
     
        
</code>

##Whois
The Damaged Earth Catalog is a project by Marloes de Valk in the context of their PhD research project at the [Centre for the Study of the Networked Image](https://www.centreforthestudyof.net/) in collaboration with [The Photographers' Gallery](https://thephotographersgallery.org.uk/image-end-world).
